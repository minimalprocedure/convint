Convint - example of an integer converter in many bases
===

application: Convint - example of an integer converter in many bases  
module: conversions.ml  
version: 1  
year: 2017  
developer: Massimo Ghisalberti  
email: zairik@gmail.com  
licence: GPL3  
